package openam

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.request.StringBody

class PLLPolicyTest extends Simulation {

  var httpProtocol = http
    .baseURL("http://local.forrest.org:8080")
    .contentTypeHeader("application/json")
    .disableCaching

  def getScenario(): ScenarioBuilder = {
    scenario("Policy Performance test")
      .exec(http("Login agent")
        .post("/openam/json/authenticate")
        .disableUrlEncoding
        .headers(Map("X-OpenAM-Username" -> "webagent", "X-OpenAM-Password" -> "qwerqwer"))
        .body(StringBody("{}"))
        .check(status.is(200))
        .check(jsonPath("$.tokenId").find.saveAs("token_agent")))
      .exec(http("Login user1")
        .post("/openam/json/authenticate")
        .disableUrlEncoding
        .headers(Map("X-OpenAM-Username" -> "demo", "X-OpenAM-Password" -> "changeit"))
        .body(StringBody("{}"))
        .check(status.is(200))
        .check(jsonPath("$.tokenId").find.saveAs("token_user")))
      .during(300) {
        exec(http("Evaluate Policy")
          .post("/openam/policyservice")
          .headers(Map("iPlanetDirectoryPro" -> "${token_agent}"))
          .body(ELFileBody("pllrequest.xml"))
          .check(status.is(200))
          .check(regex( """<Value>allow</Value>""").count.is(2)))
      }
  }

  setUp(getScenario().inject(atOnceUsers(30)).protocols(httpProtocol))
}