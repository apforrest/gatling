package openam

import java.util.UUID

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.StringBody

import scala.concurrent.duration._

class ExerciseEvaluation extends Simulation {

  var maxCount = 100

  var httpProtocol = http
    .baseURL("http://local.forrest.org:8080")
    .contentTypeHeader("application/json")
    .disableCaching
    .disableUrlEncoding

  object Authn {

    var admin = exec(http("Authenticating admin")
      .post("/openam/json/authenticate")
      .headers(Map("X-OpenAM-Username" -> "amadmin", "X-OpenAM-Password" -> "qwerqwer"))
      .body(StringBody("{}"))
      .check(status.is(200))
      .check(jsonPath("$.tokenId").find.saveAs("admin_token")))

    var user = exec(http("Authenticating user")
      .post("/openam/json/authenticate")
      .headers(Map("X-OpenAM-Username" -> "user${uniqueKey}", "X-OpenAM-Password" -> "qwerqwer"))
      .body(StringBody("{}"))
      .check(status.is(200))
      .check(jsonPath("$.tokenId").find.saveAs("user_token")))

  }

  object Logout {

    var admin = exec(http("Logging out admin")
      .post("/openam/json/sessions?_action=logout")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .check(status.is(200)))

    var user = exec(http("Logging out user")
      .post("/openam/json/sessions?_action=logout")
      .headers(Map("iPlanetDirectoryPro" -> "${user_token}"))
      .check(status.is(200)))

  }

  object User {

    var create = exec(http("Creating user")
      .post("/openam/json/users?_action=create")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .body(ELFileBody("userdefinition.json"))
      .check(status.is(201)))

    var delete = exec(http("Removing user")
      .delete("/openam/json/users/user${uniqueKey}")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .check(status.is(200)))

  }

  object Group {

    var create = exec(http("Creating group")
      .post("/openam/json/groups?_action=create")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .body(ELFileBody("groupdefinition.json"))
      .check(status.is(201)))

    var delete = exec(http("Removing group")
      .delete("/openam/json/groups/group${uniqueKey}")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .check(status.is(200)))

  }

  object Policy {

    var create = exec(http("Creating policy")
      .post("/openam/json/policies?_action=create")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .body(ELFileBody("policydefinition.json"))
      .check(status.is(201)))

    var delete = exec(http("Removing policy")
      .delete("/openam/json/policies/policy${uniqueKey}")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .check(status.is(200)))

    var evaluate = exec(http("Evaluating policy")
      .post("/openam/json/policies?_action=evaluate")
      .headers(Map("iPlanetDirectoryPro" -> "${admin_token}"))
      .body(ELFileBody("restrequest.json"))
      .check(status.is(200))
      .check(jsonPath("$[0].actions.GET").is("true"))
    )

  }

  val feeder = Iterator.continually(Map("uniqueKey" -> UUID.randomUUID()))

  var performanceScenario = scenario("setup")
    .feed(feeder)
    .exec(Authn.admin)
    .exec(User.create)
    .exec(Group.create)
    .exec(Policy.create)
    .exec(Authn.user)
    .during(3 minutes) {
      Policy.evaluate
    }
    .exec(Logout.user)
    .exec(Policy.delete)
    .exec(Group.delete)
    .exec(User.delete)
    .exec(Logout.admin)

  setUp(performanceScenario.inject(atOnceUsers(50))).protocols(httpProtocol)

}