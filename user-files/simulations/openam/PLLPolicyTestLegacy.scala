package openam

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.request.StringBody

class PLLPolicyTestLegacy extends Simulation {

  var httpProtocol = http
    .baseURL("http://local.forrest.org:8080")
    .contentTypeHeader("application/json")
    .disableCaching

  def getScenario(): ScenarioBuilder = {
    scenario("Policy Performance test")
      .exec(http("Login agent")
        .post("/openam/identity/authenticate")
        .disableUrlEncoding
        .queryParamMap(Map("username" -> "webagent", "password" -> "qwerqwer"))
        .body(StringBody("{}"))
        .check(status.is(200))
        .check(regex("token.id=(.*)").find.saveAs("token_agent")))
      .exec(http("Login user1")
        .post("/openam/identity/authenticate")
        .disableUrlEncoding
        .queryParamMap(Map("username" -> "demo", "password" -> "changeit"))
        .body(StringBody("{}"))
        .check(status.is(200))
        .check(regex("token.id=(.*)").find.saveAs("token_user")))
      .during(300) {
        exec(http("Evaluate Policy")
          .post("/openam/policyservice")
          .headers(Map("iPlanetDirectoryPro" -> "${token_agent}"))
          .body(ELFileBody("pllrequest.xml"))
          .check(status.is(200))
          .check(regex( """<Value>allow</Value>""").count.is(2)))
      }
  }

  setUp(getScenario().inject(atOnceUsers(100)).protocols(httpProtocol))
}