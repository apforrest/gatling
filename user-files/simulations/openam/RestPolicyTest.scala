package openam

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.request.StringBody

class RestPolicyTest extends Simulation {

  var httpProtocol = http
    .baseURL("http://local.forrest.org:8080")
    .contentTypeHeader("application/json")
    .disableCaching

  def getScenario(): ScenarioBuilder = {
    scenario("Policy Performance test")
      .exec(http("Login agent")
      .post("/openam/json/authenticate")
      .disableUrlEncoding
      .headers(Map("X-OpenAM-Username" -> "amadmin", "X-OpenAM-Password" -> "qwerqwer"))
      .body(StringBody("{}"))
      .check(status.is(200))
      .check(jsonPath("$.tokenId").find.saveAs("token_agent")))
      .exec(http("Login user1")
      .post("/openam/json/authenticate")
      .disableUrlEncoding
      .headers(Map("X-OpenAM-Username" -> "user1", "X-OpenAM-Password" -> "qwerqwer"))
      .body(StringBody("{}"))
      .check(status.is(200))
      .check(jsonPath("$.tokenId").find.saveAs("token_user")))
      .during(300) {
      exec(http("Evaluate policy via REST")
        .post("/openam/json/policies?_action=evaluate")
        .headers(Map("iPlanetDirectoryPro" -> "${token_agent}"))
        .body(ELFileBody("restrequest.json"))
        .check(status.is(200))
        .check(jsonPath("$[0].actions.GET").is("true")))
    }
  }

  setUp(getScenario().inject(atOnceUsers(100)).protocols(httpProtocol))
}